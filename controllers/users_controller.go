package controllers

import (
	"mvc/services"
	"mvc/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetUser(c *gin.Context) {
	// c.query('user_id')
	userID, err := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if err != nil {
		userErr := &utils.ApplicationError{
			Message:    "ID Pengguna harus berupa nomor",
			StatusCode: http.StatusBadRequest,
			Code:       "Bad Request",
		}
		c.JSON(userErr.StatusCode, userErr)
		return
	}

	user, userErr := services.GetUser(userID)
	if userErr != nil {
		c.JSON(userErr.StatusCode, userErr)
		return
	}

	c.JSON(http.StatusOK, user)
}
