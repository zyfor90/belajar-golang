package domain

import (
	"fmt"
	"mvc/utils"
	"net/http"
)

var (
	users = map[int64]*User{
		123: {
			Id:        123,
			Firstname: "Febriansyah",
			Lastname:  "Bambang",
			Email:     "febriansyah602@gmail.com",
		},
		124: {
			Id:        124,
			Firstname: "Bambang",
			Lastname:  "Test",
			Email:     "bambangtest@gmail.com",
		},
	}
)

func GetUser(userID int64) (*User, *utils.ApplicationError) {

	if user := users[userID]; user != nil {
		return user, nil
	}

	return nil, &utils.ApplicationError{
		Message:    fmt.Sprintf("ID Pengguna %v tidak ditemukan!.", userID),
		StatusCode: http.StatusNotFound,
		Code:       "Not Found",
	}

	// return nil, errors.New(fmt.Sprintf("User %v was not found", userID))
}
